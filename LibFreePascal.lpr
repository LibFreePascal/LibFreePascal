program LibFreePascal;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces,
  Forms,
  System.Time.Timer.TimerFDTimer,
  System.Time.Tests.TimerTests;

{$R *.res}

var
  TimerFD: TFileDescriptor;
  InterSpec, OldSpec: TIntervalTimerSpecification;
  Now: TTimeSpecification;
  NElapsed: LongInt;

  Timer: TTimerFDTimer;
  Tests: TTimerTests;

begin
  TimerFD := timerfd_create(CLOCK_RealTime, 0);
  WriteLn(TimerFD);

  clock_gettime(CLOCK_Monotonic, @Now);
  WriteLn('Now.tv_sec := ', Now.tv_sec);
  WriteLn('Now.tv_nsec := ', Now.tv_nsec);

  InterSpec.it_interval.tv_sec := 10;
  InterSpec.it_interval.tv_nsec := 250000000;

  InterSpec.it_value.tv_sec := Now.tv_sec + 1;
  InterSpec.it_value.tv_nsec := 0;

  timerfd_settime(TimerFD, 0, @InterSpec, @OldSpec);

  WriteLn('OldSpec.it_interval.tv_nsec := ', OldSpec.it_interval.tv_nsec);
  timerfd_gettime(TimerFD, @OldSpec);
  WriteLn('OldSpec.it_interval.tv_nsec := ', OldSpec.it_interval.tv_nsec);

  timerfd_read(TimerFD, @NElapsed, SizeOf(NElapsed));

  // TODO: Run FPCUnit tests.

  {
  RequireDerivedFormResource := True;
  Application.Initialize;
  Application.Run;
  }
end.

